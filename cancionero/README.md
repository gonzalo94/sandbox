# El cancionero

Cada estudiante deberá crear un archivo con la letra de una canción. La letra 
de la canción deberá ser cargada por 2 alumnos diferentes

Al completar la letra, se deberá crear un **merge request** en este repositorio
para que sea aceptado.

## Requisitos

* El nombre del archivo será: `nombre_de_artista-nombre_de_tema.txt`
* La letra deberá escribirse en un archivo de texto sin formato

*Sólo se aceptarán las letras que respeten lo mencionado anteriormente*


## ¿Cómo proceder?

* Determinar mi grupo de tres personas(los estudiantes estan en la primer 
actividad)
* Planificar como trabajaremos (quien escribirá qué estrofa)
* Clonar este repositorio
* En el directorio cancionero crear un archivo con el nombre de un tema, por
  ejemplo: `pixies-where_is_my_mind.txt`
* Completar algunas estrofas de mi archivo
* Advertir a mis compañeros que finalicé mi parte para que puedan avanzar
  clonando mi repo 
  * Cada cada deberá clonar **mi repo** o el de **mi equipo**
  * Trabajan sobre mi tema y al finalizar hacen un merge request a **mi repo**
  * Yo acepto si es correcto
* Cuando todos mis co-workers terminan realizo un merge request al repositorio
 de la materia
* **Para finalizar y completar la actividad, cada estudiante deberá subir al
aula virtual la canción completa **

